# Visualisateur de Base de Données SQLite avec Qt

## À propos

Ce projet est une application desktop conçue avec Qt pour la visualisation et la gestion des bases de données SQLite. La première partie du TP offre aux utilisateurs une interface et la gestion des utilisateurs et de leurs profils. La seconde partie enrichit l'application avec des fonctionnalités avancées pour interagir avec les bases de données SQLite, permettant aux utilisateurs d'ajouter, de supprimer, de sélectionner, et de visualiser les bases de données et leurs contenus, ainsi que d'exécuter des requêtes SQL.

## Table des matières

- [À propos](#à-propos)
- [Prérequis](#prérequis)
- [Installation](#installation)
- [Fonctionnalités](#fonctionnalités)
- [Contribution](#contribution)
- [Construit avec](#construit-avec)

## Prérequis

- Qt 6.6.0
- Compilateur C++ 
- SQLite3
- Qt Creator pour le développement et la compilation

## Installation

Pour configurer et exécuter ce projet sur votre système :

1. Télécharger le dépôt 
2. Ouvrir le fichier CMakeLists.txt sur Qt Creator
3. Compilez et exécutez le projet directement depuis Qt Creator.

## Fonctionnalités

La seconde partie du TP ajoute les fonctionnalités suivantes :

- Ajout et suppression de bases de données existantes via un dialogue QFileDialog.
- Sélection d'une base de données pour afficher les tables qu'elle contient.
- Visualisation du contenu des tables.
- Exécution et affichage des résultats des requêtes SQL par les utilisateurs.
- Gestion des droits d'accès des utilisateurs sur les bases de données en fonction des droits dans l'application.

## Contribution

Projet développé par :

- Fontaine Nathan 
- Lomaszewicz Alexandre
- Petit Caroline

## Construit avec

- **Qt** - Le framework utilisé pour le développement de l'interface utilisateur et la logique métier.
- **SQLite** - Le système de gestion de base de données pour le stockage et la gestion des données.
