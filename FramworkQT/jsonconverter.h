#ifndef JSONCONVERTER_H
#define JSONCONVERTER_H

#include "utilisateur.h"

#include <QVector>
#include <QJsonObject>
#include <QJsonArray>
#include "droit.h"

class JsonConverter
{
public:
    JsonConverter();

    static QVector<Utilisateur> getListeUtilisateurs(const QJsonDocument& json);

    static QVector<Droit> getListeDroits(const QJsonDocument& json);

    static QJsonArray utilisateursToJson(const QVector<Utilisateur>& utilisateurs);

    static QJsonArray profisToJson(const QList<Profil>& profils);

    static QJsonArray droitsToJson(const QVector<Droit>& droits);
};

#endif // JSONCONVERTER_H
