#ifndef PROFIL_H
#define PROFIL_H

#include <vector>
#include <QString>

using namespace std;

class Profil
{
public:
    Profil();
    Profil(const QString& nom);
    Profil(const int id, const QString& nom);
    ~Profil();

    // Getters
    int getId() const;
    QString getNom() const;
    int getProfilCounter() const;

    // Setters
    void setNom(const QString& nom);
    void setProfilCounter(int number);

    //Surcharge
    bool operator==(const Profil& autre) const;
    QString toString() const;

private:
    int id;
    QString nom;
    static int profilCounter;
};

#endif // PROFIL_H
