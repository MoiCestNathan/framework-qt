#include "application.h"

QString Application::fileLocation = "save.json";

/**
 * Constructeur de la classe Application.
 * Initialise la liste des droits si elle est vide en ajoutant des droits par défaut.
 */
Application::Application() {
    QJsonArray droitArray = JsonConverter::droitsToJson(listeDroits);
    if(droitArray.isEmpty()) {
        Droit d1(0, "Administrateur");
        Droit d2(1, "Lecture");
        Droit d3(2, "Ecriture");
        listeDroits.push_back(d1);
        listeDroits.push_back(d2);
        listeDroits.push_back(d3);
    }
}

/**
 * Destructeur de la classe Application.
 */
Application::~Application() {}

/**
 * Retourne l'emplacement du fichier de sauvegarde des données.
 *
 * @return QString Chemin vers le fichier de sauvegarde.
 */
QString Application::getFileLocation() {
    return fileLocation;
}

/**
 * Retourne la liste des utilisateurs actuellement stockés.
 *
 * @return QVector<Utilisateur> Liste des utilisateurs.
 */
QVector<Utilisateur> Application::getUserList() const
{
    return listeUtilisateurs;
}

/**
 * Retourne la liste des droits disponibles dans l'application.
 *
 * @return QVector<Droit> Liste des droits.
 */
QVector<Droit> Application::getListeDroits() const
{
    return listeDroits;
}

/**
 * Définit la nouvelle liste des utilisateurs.
 *
 * @param liste Nouvelle liste des utilisateurs à stocker.
 */
void Application::setUserList(QVector<Utilisateur> liste)
{
    listeUtilisateurs = liste;
}

/**
 * Charge la liste des droits depuis le fichier de sauvegarde.
 */
void Application::setListeDroits() {
    QJsonDocument doc = Parseur::parseJsonFile(fileLocation);
    listeDroits = JsonConverter::getListeDroits(doc);
}

/**
 * Met à jour la liste des utilisateurs depuis le fichier de sauvegarde.
 */
void Application::updateUserList() {
    QJsonDocument doc = Parseur::parseJsonFile(fileLocation);
    listeUtilisateurs = JsonConverter::getListeUtilisateurs(doc);
}

/**
 * Ajoute un nouvel utilisateur et met à jour le fichier de sauvegarde.
 *
 * @param utilisateur Utilisateur à ajouter.
 * @return int Code indiquant le succès (1) ou l'échec (-1) de l'opération.
 */
int Application::ajouterUtilisateur(Utilisateur utilisateur) {
    // Ajoute l'utilisateur à la liste en mémoire des utilisateurs
    listeUtilisateurs.push_back(utilisateur);

    // Convertit la liste des utilisateurs et des droits en format JSON
    QJsonArray userArray = JsonConverter::utilisateursToJson(listeUtilisateurs);
    QJsonArray droitArray = JsonConverter::droitsToJson(listeDroits);
    QJsonObject data;
    data ["utilisateurs"] = userArray;
    data ["droits"] = droitArray;
    QJsonDocument doc (data);

    // Écrit les données JSON dans le fichier de sauvegarde
    if(jsonWriter::writeJson(doc)) {
        return 1;
    }
    return -1; // Retourne -1 en cas d'échec de l'écriture dans le fichier
}

/**
 * Met à jour les informations d'un utilisateur existant et le fichier de sauvegarde.
 *
 * @param utilisateur Utilisateur à mettre à jour.
 * @return int Code indiquant le succès (1) ou l'échec (-1) de l'opération.
 */
int Application::updateUtilisateur(Utilisateur utilisateur) {
    // Crée une nouvelle liste JSON pour les utilisateurs
    QJsonArray userArray = JsonConverter::utilisateursToJson(listeUtilisateurs);
    QJsonArray droitArray = JsonConverter::droitsToJson(listeDroits);
    QJsonArray nouvelleListe;

    // Parcourt la liste des utilisateurs pour trouver et mettre à jour l'utilisateur concerné
    for(int i = 0; i<userArray.size(); i++) {
        QJsonObject user = userArray[i].toObject();
        int idUtilisateur = user["id"].toInt();
        if(idUtilisateur == utilisateur.getId()) {
            // Met à jour les informations de l'utilisateur
            user["nom"] = utilisateur.getNom();
            user["prenom"] = utilisateur.getPrenom();
            user["pseudo"] = utilisateur.getPseudo();
            user["profilSelectionne"] = utilisateur.getProfilSelectionne();
            user["profils"] = JsonConverter::profisToJson(utilisateur.getProfils());
        }
        nouvelleListe.append(user); // Ajoute l'utilisateur mis à jour à la nouvelle liste
    }
    QJsonObject data;
    data ["utilisateurs"] = nouvelleListe;
    data ["droits"] = droitArray;
    QJsonDocument doc (data);

    // Écrit les données mises à jour dans le fichier JSON
    if(jsonWriter::writeJson(doc)) {
        return 1;
    }
    return -1;
}
