#include "connexion.h"
#include "inscription.h"
#include "parseur.h"
#include "jsonconverter.h"
#include "session.h"
#include "application.h"
#include <QApplication>
#include "parseur.h"
#include "jsonconverter.h"
#include "utilisateur.h"
#include "jsonwriter.h"
#include "mainwindow.h"
#include <QMainWindow>

int main(int argc, char *argv[])
{
    qDebug() << "Lancement application";
    QApplication a(argc, argv);
    QFile styleFile("../FramworkQT/globalStyle.qss");
    if (styleFile.open(QFile::ReadOnly | QFile::Text)) {
        QString styleSheet = QLatin1String(styleFile.readAll());
        a.setStyleSheet(styleSheet);
        styleFile.close();
    }
    MainWindow w;
    w.setWindowTitle("Visualisateur de base de données ");
    w.show();
    return a.exec();
}
