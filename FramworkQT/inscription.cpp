#include "inscription.h"
#include "ui_inscription.h"
#include "utilisateur.h"
#include "accueil.h"
#include "session.h"

#include <QStackedWidget>
#include <QDebug>

#include <QCoreApplication>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>


/**
 * Constructeur de la classe Inscription.
 * Initialise l'interface utilisateur pour la fenêtre d'inscription.
 *
 * @param parent Le widget parent de cette fenêtre d'inscription.
 */
Inscription::Inscription(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Inscription)
{
    ui->setupUi(this);
    QVector<Utilisateur> userList = JsonConverter::getListeUtilisateurs(Parseur::parseJsonFile(Application::getFileLocation()));
    if(userList.length() < 2){
        ui->haveAccountPushButton->setVisible(false);
    }
}

/**
 * Destructeur de la classe Inscription.
 * Nettoie l'interface utilisateur lors de la destruction de la fenêtre.
 */
Inscription::~Inscription()
{
    delete ui;
}

/**
 * Slot activé lors du clic sur le bouton d'inscription.
 * Vérifie la validité des champs d'inscription et crée un nouvel utilisateur si les conditions sont remplies.
 */
void Inscription::on_signinButton_clicked()
{
    // Récupération des données du formulaire
    QString firstName = ui->firstName->text();
    QString lastName = ui->lastName->text();
    QString userName = ui->userName->text();
    QString password = ui->password->text();
    QString confirmPassword = ui->confirmPassword->text();

    // Vérification de la complétude du formulaire
    if (firstName.isEmpty() || lastName.isEmpty() || userName.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()){
        qDebug() << "Erreur de saisie";
        QMessageBox::warning(this, "Erreur d'inscription", "Erreur de saisie");

        return;
    }
    // Vérification de la concordance des mots de passe
    if (password != confirmPassword){
        qDebug() << "Mots de passe différents";
        QMessageBox::warning(this, "Erreur d'inscription", "Mots de passe différents");
        return;
    }

    // Créer un nouvel utilisateur
    if(Application::getApplicationinstance().ajouterUtilisateur(Utilisateur(lastName, firstName, userName, password)) == 1) {
        // L'utilisateur a été créé avec succès
        qDebug() << "L'utilisateur a été créé avec succès.";
        QMessageBox::information(this, "Inscription réussi", "L'utilisateur a été créé avec succès.");

        ui->firstName->clear();
        ui->lastName->clear();
        ui->userName->clear();
        ui->password->clear();
        ui->confirmPassword->clear();

        changerPage(1);

    } else {
        qDebug() << "Erreur : l'utilisateur n'a pas pu être créé.";
        QMessageBox::warning(this, "Erreur d'inscription", "Erreur : l'utilisateur n'a pas pu être créé.");
    }
}

void Inscription::on_haveAccountPushButton_clicked()
{
    //QThread::msleep(50);
    changerPage(1);
}
