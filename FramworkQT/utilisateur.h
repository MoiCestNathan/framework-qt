#ifndef UTILISATEUR_H
#define UTILISATEUR_H

#include <QString>
#include <QList>
#include "profil.h"

class Profil;

class Utilisateur
{
private:
    static int idCounter;
    int id;
    QString nom;
    QString prenom;
    QString pseudo;
    QString mdp;
    QList<Profil> profils;
    int profilSelectionne;
    int idDroit;

public:
    Utilisateur();

    Utilisateur(const QString& nom, const QString& prenom, const QString& pseudo, const QString& mdp);

    Utilisateur(int id, const QString& nom, const QString& prenom, const QString& pseudo, const QString& mdp);
    ~Utilisateur();

    // Getters
    int getId() const;
    QString getNom() const;
    QString getPrenom() const;
    QString getPseudo() const;
    QString getMdp() const;
    int getIdCounter() const;
    int getProfilSelectionne() const;
    int getIdDroit() const;
    QList<Profil> getProfils() const;

    // Setters
    void setId(int id);
    void setNom(const QString& nom);
    void setPrenom(const QString& prenom);
    void setPseudo(const QString& pseudo);
    void setMdp(const QString& mdp);
    void setIdCounter(int number);
    void setProfilSelectionne(int number);
    void setIdDroit(int id);
    void setProfils(QList<Profil> profils);

    void ajouterProfil(Profil profil);
    void supprimerProfil(Profil profil);

};

#endif // UTILISATEUR_H
