#include "jsonwriter.h"


/**
 * Constructeur par défaut de la classe jsonWriter.
 */
jsonWriter::jsonWriter() {}

/**
 * Écrit un document JSON dans le fichier de configuration spécifié par Application::getFileLocation().
 *
 * @param jsonDocument Le document JSON à écrire dans le fichier.
 * @return int Retourne 1 si le fichier a été écrit avec succès, sinon -1 si une erreur s'est produite lors de l'ouverture du fichier.
 */
int jsonWriter::writeJson(const QJsonDocument& jsonDocument) {
    QFile jsonFile(Application::getFileLocation());
    if (jsonFile.open(QIODevice::WriteOnly)) {
        QJsonDocument jsonDoc(jsonDocument);
        jsonFile.write(jsonDoc.toJson());
        jsonFile.close();
        return 1;
    }else {
        return -1;
    }
}
