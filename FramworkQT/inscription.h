#ifndef INSCRIPTION_H
#define INSCRIPTION_H

#include <QMessageBox>
#include <QWidget>
#include "utilisateur.h"
#include "ui_inscription.h"
#include <QThread>

namespace Ui {
class Inscription;
}

class Inscription : public QWidget
{
    Q_OBJECT

public:
    explicit Inscription(QWidget *parent = nullptr);
    ~Inscription();

signals:
    void changerPage(int index);

private slots:
    void on_signinButton_clicked();
    void on_haveAccountPushButton_clicked();

private:
    Ui::Inscription *ui;
};

#endif // INSCRIPTION_H
