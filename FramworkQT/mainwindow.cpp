#include "mainwindow.h"
#include "ui_mainwindow.h"

Inscription *cInscription;
Connexion *cConnexion;
Accueil *cAccueil;

/**
 * Constructeur de la classe MainWindow.
 * Initialise l'interface principale de l'application, instancie les widgets des différentes fenêtres (inscription, connexion, accueil),
 * et gère la navigation entre ces fenêtres en utilisant un QStackedWidget.
 *
 * @param parent Le widget parent de cette fenêtre principale.
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this); // Initialise l'interface utilisateur

    stackedWidget = new QStackedWidget(this); // Crée un stacked widget pour gérer la navigation

    // Crée et configure le widget d'inscription
    QWidget *widgetInscription = new QWidget;
    uiInscription.setupUi(widgetInscription);
    cInscription = new Inscription(widgetInscription);

    // Crée et configure le widget de connexion
    QWidget *widgetConnexion = new QWidget;
    uiConnexion.setupUi(widgetConnexion);
    cConnexion = new Connexion(widgetConnexion);

    // Crée et configure le widget d'accueil
    QWidget *widgetAccueil = new QWidget;
    uiAccueil.setupUi(widgetAccueil);
    cAccueil = new Accueil(widgetAccueil);

    // Ajoute les widgets au stacked widget
    stackedWidget->insertWidget(0, widgetInscription);
    stackedWidget->insertWidget(1, widgetConnexion);
    stackedWidget->insertWidget(2, widgetAccueil);

    // Connecte les signaux pour la navigation entre les pages
    connect(cConnexion, &Connexion::changerPage, this, &MainWindow::afficherPage);
    connect(cInscription, &Inscription::changerPage, this, &MainWindow::afficherPage);
    connect(cAccueil, &Accueil::changerPage, this, &MainWindow::afficherPage);

    // Charge la liste des utilisateurs depuis le fichier JSON
    QVector<Utilisateur> userList = JsonConverter::getListeUtilisateurs(Parseur::parseJsonFile(Application::getFileLocation()));

    // Crée un utilisateur admin par défaut si aucun utilisateur n'existe
    if(userList.length() < 1) {
        Utilisateur admin(1, "admin", "admin", "admin", "password");
        admin.setIdDroit(1);
        userList.push_back(admin);
    }

    // Configure la liste des utilisateurs dans l'application
    Application::getApplicationinstance().setUserList(userList);
    if(userList.length() > 1) {
        stackedWidget->setCurrentIndex(1);
    } else {
        stackedWidget->setCurrentIndex(0);
    }

    // Définit la page initiale en fonction du nombre d'utilisateurs
    setCentralWidget(stackedWidget);
}

/**
 * Destructeur de la classe MainWindow.
 * Nettoie les ressources utilisées par l'interface utilisateur.
 */
MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * Change l'affichage actuel dans le QStackedWidget pour afficher la fenêtre correspondante à l'index spécifié.
 * Met à jour l'affichage de la page d'accueil si nécessaire.
 *
 * @param index L'index de la page à afficher dans le QStackedWidget.
 */
void MainWindow::afficherPage(int index)
{
    stackedWidget->setCurrentIndex(index);
    switch(index) {
    case 2:
        cAccueil->updatePage();
        break;
    }
}
