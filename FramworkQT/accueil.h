#ifndef ACCUEIL_H
#define ACCUEIL_H

#include <QWidget>
#include <QLabel>
#include <QStringListModel>
#include "application.h"
#include "session.h"
#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QStringListModel>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QStandardItem>
#include <QRegularExpression>

namespace Ui {
class Accueil;
}

class Accueil : public QWidget
{
    Q_OBJECT

public:
    explicit Accueil(QWidget *parent = nullptr);
    ~Accueil();
    void updatePage();
    void afficherStructureBDD(const QString &nomBDD);
    QString getCurrentDisplayedDbName() const;

signals:
    void changerPage(int index);

private slots:
    void on_logoutButton_clicked();
    void on_addProfil_clicked();
    void on_profil_changed(int id);

    void on_addDatabaseButton_clicked();
    void on_deleteDatabaseButton_clicked();

    void on_AppliquerpushButton_clicked();

    void on_listViewDB_clicked(const QModelIndex &index);
    void on_tableComboBox_currentIndexChanged(int index);

private:
    Ui::Accueil *ui;

    QLabel *NomProfil;

    QStringListModel *dbListModel;

    QMap<QString, QString> dbPathMap;

    QString currentDisplayedDbName;
};

#endif // ACCUEIL_H
