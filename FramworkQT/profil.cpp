#include "profil.h"

int Profil::profilCounter = 0;

/**
* Constructeur par défaut. Initialise un profil sans nom avec un ID unique.
*/
Profil::Profil() {}

/**
* Constructeur avec nom.
* Initialise le profil avec un identifiant unique et le nom spécifié.
*
* @param nom Le nom du profil.
*/
Profil::Profil(const QString& nom) {
    this->id = profilCounter;
    this->nom = nom;
    profilCounter++;
}

/**
 * Constructeur de Profil avec identifiant et nom.
 * Ajuste le compteur d'identifiant de profil si nécessaire.
 *
 * @param id L'identifiant du profil.
 * @param nom Le nom du profil.
 */
Profil::Profil(const int id, const QString& nom) {
    this->id = id;
    this->nom = nom;
    if(profilCounter <= id) {
        profilCounter = id+1;
    }
}

/**
 * Destructeur de la classe Profil.
 */
Profil::~Profil() {}

/**
 * Retourne l'identifiant du profil.
 *
 * @return int L'identifiant du profil.
 */
int Profil::getId() const {
    return this->id;
}

/**
 * Définit le nom du profil.
 *
 * @param nom Le nouveau nom du profil.
 */
QString Profil::getNom() const {
    return this->nom;
}

/**
* Définit le nom du profil.
*
* @param nom Le nouveau nom du profil.
*/
int Profil::getProfilCounter() const {
    return this->profilCounter;
}

/**
 * Définit le nom du profil.
 *
 * @param nom Le nouveau nom du profil.
 */
void Profil::setNom(const QString& nom) {
    this->nom = nom;
}

/**
 * Définit le compteur de profil, principalement pour des raisons de restauration ou de synchronisation.
 *
 * @param number La nouvelle valeur du compteur de profil.
 */
void Profil::setProfilCounter(int number) {
    this->profilCounter = number;
}

/**
 * Surcharge de l'opérateur '==' pour comparer deux profils.
 * Cette méthode compare les noms de deux instances de `Profil` pour déterminer si elles sont équivalentes.
 *
 * @param autre Une référence constante à une autre instance de `Profil` à comparer.
 * @return bool Retourne `true` si les noms des deux profils sont identiques, sinon `false`.
 */
bool Profil::operator==(const Profil& autre) const {
    return nom == autre.nom;
}

/**
 * Retourne une représentation en chaîne de caractères du profil, actuellement juste le nom du profil.
 *
 * @return String Le nom du profil.
 */
QString Profil::toString() const {
    return nom;
}
