#ifndef SESSION_H
#define SESSION_H

#include <QSettings>
#include <QMap>
#include "utilisateur.h"
#include "application.h"
#include "jsonconverter.h"

class Session
{
private:

    Utilisateur connectedUser;

    Session();
    ~Session();
    Session(const Session&) = delete;
    Session& operator=(const Session&) = delete;

public:
    static Session& getInstanceSession() {
        static Session instance;
        return instance;
    }

    void setConnectedUser(const Utilisateur &user);

    Utilisateur getConnectedUser() const;

    void clearUser();

    bool login(const QString& pseudo, const QString& mdp);

    bool logout();

    void updateUser();
};

#endif // SESSION_H
