#include "jsonconverter.h"
#include "qjsondocument.h"

/**
 * Constructeur par défaut de la classe JsonConverter.
 */
JsonConverter::JsonConverter() {}

/**
 * Convertit un document JSON en un QVector d'utilisateurs.
 *
 * @param json Document JSON contenant les données des utilisateurs.
 * @return QVector<Utilisateur> Liste des utilisateurs extraits du document JSON.
 */
QVector<Utilisateur> JsonConverter::getListeUtilisateurs(const QJsonDocument& json)
{
    QVector<Utilisateur> listeUtilisateurs;

    if (json.isObject ()) {
        QJsonObject jsonObject = json.object ();
        if (jsonObject.contains ("utilisateurs") && jsonObject ["utilisateurs"].isArray ()) {
            QJsonArray users = jsonObject ["utilisateurs"].toArray ();

            for(int i = 0; i < users.size(); i++) {
                QJsonObject user = users[i].toObject();

                int id = user["id"].toInt();
                QString nom = user["nom"].toString();
                QString prenom = user["prenom"].toString();
                QString pseudo = user["pseudo"].toString();
                QString mdp = user["mdp"].toString();
                int selectedProfil = user["profilSelectionne"].toInt();
                int idDroit = user["idDroit"].toInt();

                Utilisateur u(id, nom, prenom, pseudo, mdp);
                u.setIdDroit(idDroit);

                u.setProfilSelectionne(selectedProfil);

                QList<Profil> profils;
                QJsonArray profilsArray = user["profils"].toArray();
                qDebug() << profilsArray.size();
                for(int j = 0; j < profilsArray.size(); j++) {
                    QJsonObject profil = profilsArray[j].toObject();
                    int idProfil = profil["id"].toInt();
                    QString nomProfil = profil["nom"].toString();
                    Profil p(idProfil, nomProfil);
                    profils.append(p);
                }

                u.setProfils(profils);

                listeUtilisateurs.push_back(u);
            }
        } else {
            qWarning () << "L'objet JSON n'a pas de clé \"utilisateurs\" ou sa valeur n'est pas un tableau";
        }
    } else {
        qWarning () << "Le document JSON n'est pas un objet";
    }

    return listeUtilisateurs;
}

/**
 * Convertit un document JSON en un QVector de droits.
 *
 * @param json Document JSON contenant les données des droits.
 * @return QVector<Droit> Liste des droits extraits du document JSON.
 */
QVector<Droit> JsonConverter::getListeDroits(const QJsonDocument& json) {
    QVector<Droit> listeDroits;

    if (json.isObject ()) {
        QJsonObject jsonObject = json.object ();
        if (jsonObject.contains ("droits") && jsonObject ["droits"].isArray ()) {
            QJsonArray droits = jsonObject ["droits"].toArray ();

            for(int i = 0; i < droits.size(); i++) {
                QJsonObject droit = droits[i].toObject();

                int id = droit["id"].toInt();
                QString nom = droit["nom"].toString();
                Droit d(id, nom);

                listeDroits.push_back(d);
            }
        } else {
            qWarning () << "L'objet JSON n'a pas de clé \"droits\" ou sa valeur n'est pas un tableau";
        }
    } else {
        qWarning () << "Le document JSON n'est pas un objet";
    }

    return listeDroits;

}

/**
 * Convertit une liste d'utilisateurs en un tableau JSON.
 *
 * @param utilisateurs Liste des utilisateurs à convertir.
 * @return QJsonArray Tableau JSON contenant les données des utilisateurs.
 */
QJsonArray JsonConverter::utilisateursToJson(const QVector<Utilisateur>& utilisateurs) {
    QJsonArray jsonArray;
    for(const Utilisateur& utilisateur : utilisateurs) {
        QJsonObject userObject;
        userObject["id"] = utilisateur.getId();
        userObject["nom"] = utilisateur.getNom();
        userObject["prenom"] = utilisateur.getPrenom();
        userObject["pseudo"] = utilisateur.getPseudo();
        userObject["mdp"] = utilisateur.getMdp();
        userObject["profilSelectionne"] = utilisateur.getProfilSelectionne();
        userObject["profils"] = JsonConverter::profisToJson(utilisateur.getProfils());
        userObject["idDroit"] = utilisateur.getIdDroit();

        jsonArray.append(userObject);
    }

    return jsonArray;
}

/**
 * Convertit une liste de profils en un tableau JSON.
 *
 * @param profils Liste des profils à convertir.
 * @return QJsonArray Tableau JSON contenant les données des profils.
 */
QJsonArray JsonConverter::profisToJson(const QList<Profil>& profils) {
    QJsonArray jsonArray;
    for(const Profil& profil : profils) {
        QJsonObject profilObject;
        profilObject["id"] = profil.getId();
        profilObject["nom"] = profil.getNom();

        jsonArray.append(profilObject);
    }

    return jsonArray;
}


/**
 * Convertit une liste de droits en un tableau JSON.
 *
 * @param droits Liste des droits à convertir.
 * @return QJsonArray Tableau JSON contenant les données des droits.
 */
QJsonArray JsonConverter::droitsToJson(const QVector<Droit>& droits) {
    QJsonArray jsonArray;
    for(const Droit& droit : droits) {
        QJsonObject droitObject;
        droitObject["id"] = droit.getId();
        droitObject["nom"] = droit.getNom();

        jsonArray.append(droitObject);
    }

    return jsonArray;
}
