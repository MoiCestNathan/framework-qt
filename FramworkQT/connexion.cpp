#include "connexion.h"


/**
 * Constructeur de la classe Connexion.
 * Initialise l'interface utilisateur de la fenêtre de connexion.
 *
 * @param parent Le widget parent de cette fenêtre de connexion.
 */
Connexion::Connexion(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Connexion)
{
    ui->setupUi(this);
        //connect(ui->loginButton, SIGNAL(clicked()), this, SLOT(on_loginButton_clicked()));
}

/**
 * Destructeur de la classe Connexion.
 * Nettoie l'interface utilisateur lors de la destruction de la fenêtre.
 */
Connexion::~Connexion()
{
    delete ui;
}

/**
 * Slot activé lors du clic sur le bouton de connexion.
 * Vérifie les champs de nom d'utilisateur et de mot de passe, affiche un message d'erreur si l'un des champs est vide,
 * ou tente de se connecter avec les informations fournies.
 */
void Connexion::on_loginButton_clicked(){
    // Récupère le nom d'utilisateur et le mot de passe depuis l'interface
    QString userName = ui->userName->text();
    QString password = ui->password->text();


    // Vérifie si les champs sont vides et affiche un message d'erreur le cas échéant
    if (userName.isEmpty() || password.isEmpty()) {
        QMessageBox::warning(this, "Erreur de connexion", "Le nom d'utilisateur et le mot de passe ne peuvent pas être vides.");
        return;
    }

    // Tente de se connecter avec les informations fournies et affiche un message selon le résultat
    bool success = Session::getInstanceSession().login(userName, password);

    if (success) {
        // Si la connexion réussit, change de page et nettoie les champs
        changerPage(2);
        ui->userName->clear();
        ui->password->clear();
    } else {
        QMessageBox::critical(this, "Erreur de connexion", "L'identifiant ou le mot de passe est incorrect");
    }
}

void Connexion::on_createAccountButton_clicked(){
    //QThread::msleep(50);
    changerPage(0);
}
