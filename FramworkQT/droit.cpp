#include "droit.h"

/**
 * Constructeur par défaut de la classe Droit.
 */
Droit::Droit() {}

/**
 * Constructeur de la classe Droit avec initialisation.
 *
 * @param id Identifiant unique du droit.
 * @param nom Nom du droit.
 */
Droit::Droit(int id, const QString& nom):
    id(id), nom(nom) {}

/**
 * Destructeur de la classe Droit.
 */
Droit::~Droit() {}

/**
 * Retourne l'identifiant du droit.
 *
 * @return int L'identifiant du droit.
 */
int Droit::getId() const {
    return this->id;
}

/**
 * Retourne le nom du droit.
 *
 * @return QString Le nom du droit.
 */
QString Droit::getNom() const {
    return this->nom;
}

/**
 * Définit le nom du droit.
 *
 * @param nom Le nouveau nom du droit.
 */
void Droit::setNom(const QString& nom) {
    this->nom = nom;
}
