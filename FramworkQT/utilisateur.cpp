#include "utilisateur.h"

int Utilisateur::idCounter = 1;


/**
 * Constructeur par défaut de la classe Utilisateur.
 * Initialise un nouvel utilisateur sans informations.
 */
Utilisateur::Utilisateur()
{
}

/**
 * Constructeur de la classe Utilisateur avec informations.
 * Initialise un nouvel utilisateur avec un nom, prénom, pseudo et mot de passe.
 *
 * @param nom Le nom de l'utilisateur.
 * @param prenom Le prénom de l'utilisateur.
 * @param pseudo Le pseudo de l'utilisateur.
 * @param mdp Le mot de passe de l'utilisateur.
 */
Utilisateur::Utilisateur(const QString& nom, const QString& prenom, const QString& pseudo, const QString& mdp)
    : id(idCounter), nom(nom), prenom(prenom), pseudo(pseudo), mdp(mdp)
{
    Profil profil("Defaut");
    QList<Profil> profils;
    ajouterProfil(profil);
    idCounter++;
    profilSelectionne = 0;
    idDroit = 1;
}

/**
 * Constructeur de la classe Utilisateur avec identifiant spécifié.
 * Initialise un nouvel utilisateur avec un identifiant, nom, prénom, pseudo et mot de passe spécifiques.
 *
 * @param id L'identifiant unique de l'utilisateur.
 * @param nom Le nom de l'utilisateur.
 * @param prenom Le prénom de l'utilisateur.
 * @param pseudo Le pseudo de l'utilisateur.
 * @param mdp Le mot de passe de l'utilisateur.
 */
Utilisateur::Utilisateur(int id, const QString& nom, const QString& prenom, const QString& pseudo, const QString& mdp)
    : id(id), nom(nom), prenom(prenom), pseudo(pseudo), mdp(mdp)
{
    Profil profil("Defaut");
    ajouterProfil(profil);
    profilSelectionne = 0;
    if(idCounter <= id) {
        idCounter = id + 1;
    }
    idDroit = 1;
}

/**
 * Destructeur de la classe Utilisateur.
 */
Utilisateur::~Utilisateur()
{
    //qDeleteAll(profils);
}

// Getters
int Utilisateur::getId() const
{
    return id;
}

QString Utilisateur::getNom() const
{
    return nom;
}

QString Utilisateur::getPrenom() const
{
    return prenom;
}

QString Utilisateur::getPseudo() const
{
    return pseudo;
}

QString Utilisateur::getMdp() const
{
    return mdp;
}

int Utilisateur::getIdCounter() const
{
    return idCounter;
}

int Utilisateur::getProfilSelectionne() const {
    return profilSelectionne;
}

int Utilisateur::getIdDroit()  const {
    return idDroit;
}

QList<Profil> Utilisateur::getProfils() const {
    return profils;
}

// Setters
void Utilisateur::setId(int id)
{
    this->id = id;
}

void Utilisateur::setNom(const QString& nom)
{
    this->nom = nom;
}

void Utilisateur::setPrenom(const QString& prenom)
{
    this->prenom = prenom;
}

void Utilisateur::setPseudo(const QString& pseudo)
{
    this->pseudo = pseudo;
}

void Utilisateur::setMdp(const QString& mdp)
{
    this->mdp = mdp;
}

void Utilisateur::setIdCounter(int number)
{
    this->idCounter = number + 1;
}

void Utilisateur::setProfils(QList<Profil> liste) {
    this->profils = liste;
}

void Utilisateur::setProfilSelectionne(int idProfil)
{
    this->profilSelectionne = idProfil;
}

void Utilisateur::setIdDroit(int id) {
    this->idDroit = id;
}


/**
 * Ajoute un profil à la liste des profils de l'utilisateur.
 *
 * @param profil Le profil à ajouter.
 */
void Utilisateur::ajouterProfil(Profil profil)
{
    this->profils.push_back(profil);
}

/**
 * Supprime un profil de la liste des profils de l'utilisateur.
 *
 * @param profil Le profil à supprimer.
 */
void Utilisateur::supprimerProfil(Profil profil)
{
    this->profils.removeOne(profil);
}
