#ifndef JSONWRITER_H
#define JSONWRITER_H

#include <QJsonDocument>
#include <QFile>
#include "application.h"

class jsonWriter
{
public:
    jsonWriter();

    ~jsonWriter();

    static int writeJson(const QJsonDocument& jsonDocument);
};

#endif // JSONWRITER_H
