#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include "data.h"

Data::Data(){}


void Data::sauvegarderUtilisateursJSON(const QList<Utilisateur*>& utilisateurs) {
    QFile file("utilisateurs.json");
    if(!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Erreur ouverture utilisateurs.json";
        return;
    }

    QJsonArray usersArray;
    for(const Utilisateur* utilisateur : utilisateurs) {
        QJsonObject utilisateurObject;
        // Ajouter les propriétés de l'utilisateur à userObject
        usersArray.append(utilisateurObject);
    }

    QJsonDocument doc(usersArray);
    file.write(doc.toJson());
    file.close();
}

QList<Utilisateur*> Data::chargerUtilisateursJSON() {
    QList<Utilisateur*> utilisateurs;
    QFile file("utilisateurs.json");

    if(!file.open(QIODevice::ReadOnly)) {
        // Gérer l'erreur
        return utilisateurs;
    }

    QByteArray data = file.readAll();
    QJsonDocument doc(QJsonDocument::fromJson(data));
    QJsonArray usersArray = doc.array();

    for(const QJsonValue& value : usersArray) {
        QJsonObject userObject = value.toObject();
        // Lire les propriétés de l'utilisateur et les ajouter à la liste
    }

    file.close();
    return utilisateurs;
}
