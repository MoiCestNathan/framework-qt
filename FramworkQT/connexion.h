#ifndef CONNEXION_H
#define CONNEXION_H

#include <QMessageBox>
#include <QWidget>
#include "ui_connexion.h"
#include "session.h"
#include <QThread>

namespace Ui {
class Connexion;
}

class Connexion : public QWidget
{
    Q_OBJECT

public:
    explicit Connexion(QWidget *parent = nullptr);
    ~Connexion();

signals:
    void changerPage(int index);

private slots:
    void on_loginButton_clicked();
    void on_createAccountButton_clicked();

private:
    Ui::Connexion *ui;
};

#endif // CONNEXION_H
