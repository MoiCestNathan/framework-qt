#ifndef DROIT_H
#define DROIT_H

#include <QString>

class Droit
{
public:
    Droit();
    Droit(int id, const QString& nom);
    ~Droit();

    // Getters
    int getId() const;
    QString getNom() const;

    // Setters
    void setNom(const QString& nom);

private:
    int id;
    QString nom;
};

#endif // DROIT_H
