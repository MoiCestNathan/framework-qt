#ifndef DATA_H
#define DATA_H

#include <QString>
#include <QVector>

class Utilisateur;

class Data
{
public:
    Data();
    ~Data();

    // Méthodes pour sauvegarder et charger les données utilisateur
    void sauvegarderUtilisateursJSON(const QList<Utilisateur*>& utilisateurs);

    QVector<Utilisateur*> chargerUtilisateursJSON();

private:
    QString cheminFichier;
};

#endif // DATA_H
