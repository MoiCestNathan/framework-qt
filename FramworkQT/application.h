#ifndef APPLICATION_H
#define APPLICATION_H

#include <QVector>
#include <QJsonArray>
#include "Utilisateur.h"
#include "jsonconverter.h"
#include "jsonwriter.h"
#include "parseur.h"
#include "droit.h"

class Application
{
public:

    static Application& getApplicationinstance() {
        static Application instance;
        return instance;
    }

    static QString getFileLocation();

    QVector<Utilisateur> getUserList() const;
    QVector<Droit> getListeDroits() const;

    void setUserList(QVector<Utilisateur> liste);
    void updateUserList();
    void setListeDroits();

    int ajouterUtilisateur(Utilisateur utilisateur);
    int supprimerUtilisateur(Utilisateur utilisateur);
    int updateUtilisateur(Utilisateur utilisateur);

private:

    Application();
    ~Application();
    Application(const Application&) = delete;
    Application& operator=(const Application&) = delete;

    static QString fileLocation;

    QVector<Utilisateur> listeUtilisateurs; // Conteneur pour stocker les utilisateurs
    QVector<Droit> listeDroits;
};

#endif // APPLICATION_H
