#include "parseur.h"

/**
 * Analyse le contenu d'un fichier JSON et retourne un document JSON.
 * Cette méthode tente d'ouvrir et de lire le contenu du fichier de sauvegarde,
 * puis de le convertir en un QJsonDocument. Si le fichier ne peut pas être ouvert, ou si son contenu n'est pas un JSON valide,
 * la méthode retournera un QJsonDocument vide et affichera un message d'erreur.
 *
 * @param filePath Le chemin vers le fichier JSON à analyser.
 * @return QJsonDocument Le document JSON lu depuis le fichier. Retourne un document vide en cas d'échec.
 */QJsonDocument Parseur::parseJsonFile(const QString& filePath)
{
    // Créer un QJsonDocument vide
    QJsonDocument doc;

    // Ouvrir le fichier en mode lecture
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Si le fichier ne peut pas être ouvert, afficher un message d'erreur et retourner le QJsonDocument vide
        qDebug() << "Erreur: impossible d'ouvrir le fichier" << filePath;
        return doc;
    }

    // Lire tout le contenu du fichier dans une QByteArray
    QByteArray data = file.readAll();
    file.close();

    // Créer un QJsonDocument à partir de la QByteArray
    doc = QJsonDocument::fromJson(data);

    // Vérifier que le QJsonDocument n'est pas vide ou invalide
    if (doc.isEmpty() || doc.isNull())
    {
        // Si le QJsonDocument est vide ou invalide, afficher un message d'erreur et retourner le QJsonDocument vide
        qDebug() << "Erreur: le fichier" << filePath << "n'est pas un JSON valide";
        return QJsonDocument();
    }

    // Retourner le QJsonDocument
    return doc;
}

