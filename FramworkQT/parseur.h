#ifndef PARSEUR_H
#define PARSEUR_H

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

class Parseur
{
public:
    Parseur();

    ~Parseur();

    static QJsonDocument parseJsonFile(const QString& filePath);
};

#endif // PARSEUR_H
