#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>
#include <QResource>
#include "jsonconverter.h"
#include "parseur.h"
#include "application.h"
#include "ui_inscription.h"
#include "inscription.h"
#include "ui_connexion.h"
#include "connexion.h"
#include "ui_accueil.h"
#include "ui_accueil.h"
#include "accueil.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void afficherPage(int index);


private:
    Ui::MainWindow *ui;
    Ui::Inscription uiInscription;
    Ui::Connexion uiConnexion;
    Ui::Accueil uiAccueil;

    QStackedWidget *stackedWidget;
};

#endif // MAINWINDOW_H
