#include "accueil.h"
#include "ui_accueil.h"

bool unlock = false;

/**
 * Constructeur de la classe Accueil.
 * Initialise l'interface utilisateur et connecte les signaux et slots pour la gestion des interactions.
 *
 * @param parent Le widget parent de cette instance de Accueil.
 */
Accueil::Accueil(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Accueil)
{
    ui->setupUi(this);
    ui->NomProfil->clear();

    ui->tabWidget->setCurrentIndex(0);

    connect(ui->DeconnexionPushButton, SIGNAL(clicked()), this, SLOT(on_logoutButton_clicked()));
    connect(ui->ProfilsComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(on_profil_changed(int)));
    connect(ui->AjouterPushButton, SIGNAL(clicked()), this, SLOT(on_addProfil_clicked()));

    dbListModel = new QStringListModel(this);
    ui->listViewDB->setModel(dbListModel);

    ui->treeView->resizeColumnToContents(0);

    ui->treeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->treeView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->treeView->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->treeView->setAlternatingRowColors(true);

    ui->listViewDB->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listViewDB->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->listViewDB->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->listViewDB->setAlternatingRowColors(true);


    ui->tableView_2->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_2->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_2->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableView_2->setAlternatingRowColors(true);

    ui->tableViewSQL->setAlternatingRowColors(true);


    //ui->labelErrorMessage->setText("Lancement Test.");
}

/**
 * Destructeur de la classe Accueil.
 * Nettoie les ressources utilisées par l'interface utilisateur.
 */
Accueil::~Accueil()
{
    delete ui;
}

/**
 * Met à jour l'affichage de la page en fonction des informations de l'utilisateur connecté.
 * Met à jour le nom de profil, le rôle, et la liste des profils disponibles.
 */
void Accueil::updatePage()
{
    QVector<Droit> droits = Application::getApplicationinstance().getListeDroits();
    QList<QString> stringList;
    QLabel *nomProfil = ui->NomProfil;
    qDebug() << "Update" << Session::getInstanceSession().getConnectedUser().getNom();
    nomProfil->setText("Utilisateur : " + Session::getInstanceSession().getConnectedUser().getPseudo());
    int role = Session::getInstanceSession().getConnectedUser().getIdDroit();
    ui->Role->setText("Rôle : " + (droits.at(role)).getNom());

    ui->tableView_2->setModel(new QStringListModel(stringList));
    QStringList listProfils;
    for(Profil profil : Session::getInstanceSession().getConnectedUser().getProfils()) {
        listProfils.append(profil.getNom());
    }
    ui->ProfilsComboBox->clear();
    ui->ProfilsComboBox->addItems(listProfils);
    int index = Session::getInstanceSession().getConnectedUser().getProfilSelectionne();
    if(index > 0) {
        ui->ProfilsComboBox->setCurrentIndex(index);
    }
}

/**
 * Gère l'événement de clic sur le bouton de déconnexion.
 * Affiche une boîte de dialogue de confirmation avant de procéder à la déconnexion.
 */
void Accueil::on_logoutButton_clicked(){
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, tr("Confirmation de déconnexion"),
                                  tr("Êtes-vous sûr de vouloir vous déconnecter ?"),
                                  QMessageBox::Yes | QMessageBox::No);

    if (reply == QMessageBox::Yes) {
        changerPage(1);
        unlock = false;
    } else {}
}

/**
 * Gère l'événement de clic sur le bouton d'ajout de profil.
 * Ouvre une boîte de dialogue pour saisir le nom du nouveau profil et l'ajoute après confirmation.
 */
void Accueil::on_addProfil_clicked(){
    bool ok;
    QString text = QInputDialog::getText(this, tr("Ajouter un profil"),
                                         tr("Nom du profil:"), QLineEdit::Normal,
                                         "", &ok);
    if (ok && !text.isEmpty())
    {
        Profil p(text);
        Utilisateur user = Session::getInstanceSession().getConnectedUser();
        user.ajouterProfil(p);
        Application::getApplicationinstance().updateUtilisateur(user);
        Session::getInstanceSession().updateUser();
        QMessageBox::information(this, tr("Profil ajouté"),
                                 tr("Le profil '%1' a été ajouté avec succès.").arg(text));
        updatePage();
    }
    else if (!ok){}
}

/**
 * Gère le changement de sélection dans la liste déroulante des profils.
 * Met à jour le profil sélectionné pour l'utilisateur connecté.
 *
 * @param id L'index du profil sélectionné dans la liste déroulante.
 */
void Accueil::on_profil_changed(int id) {
    if(unlock) {
        Utilisateur user = Session::getInstanceSession().getConnectedUser();
        user.setProfilSelectionne(id);
        Session::getInstanceSession().setConnectedUser(user);
        Application::getApplicationinstance().updateUtilisateur(user);
    } else {
        unlock = true;
    }

}

void Accueil::on_addDatabaseButton_clicked(){
    QString filePath = QFileDialog::getOpenFileName(this, tr("Ouvrir base de données"), "", tr("Base de données SQLite (*.db *.sqlite)"));
    if (!filePath.isEmpty()) {
        QFileInfo fileInfo(filePath);
        QString fileName = fileInfo.fileName().remove(QRegularExpression("\\.db$|\\.sqlite$"));

        QStringList list = dbListModel->stringList();

        if (!list.contains(fileName, Qt::CaseInsensitive)) {
            list << fileName;
            dbListModel->setStringList(list);
            dbPathMap[fileName] = filePath;
        } else {
            QMessageBox::information(this, tr("Base de données existante"),
                                     tr("La base de données '%1' est déjà ajoutée.").arg(fileName));
        }
    }
}




void Accueil::on_deleteDatabaseButton_clicked(){
    int currentIndex = ui->listViewDB->currentIndex().row();
    if (currentIndex != -1) {
        QString dbNameToDelete = dbListModel->stringList().at(currentIndex);

        if (dbNameToDelete == getCurrentDisplayedDbName()) {
            ui->treeView->setModel(nullptr);
            ui->tableView_2->setModel(nullptr);
            ui->tableViewSQL->setModel(nullptr);
            ui->tableComboBox->clear();
            currentDisplayedDbName.clear();
        }

        QStringList list = dbListModel->stringList();
        list.removeAt(currentIndex);
        dbListModel->setStringList(list);
    }
}

QString Accueil::getCurrentDisplayedDbName() const {
    return currentDisplayedDbName;
}

void Accueil::on_AppliquerpushButton_clicked() {
    QString queryText = ui->textEditSQL->toPlainText();
    ui->labelErrorMessage->setText("Test.");
    bool autorized = false;
    int idDroit = Session::getInstanceSession().getConnectedUser().getIdDroit();

    //Découpage de la requête pour récupérer le type de requete(Select, Inset, Delete...)
    QStringList requestElements = queryText.split(" ");
    QString firstElement = requestElements.at(0).toLower();

    if (firstElement == "select") {
        autorized =  true;
    } else if (firstElement == "update" || firstElement == "delete" || firstElement == "drop" || firstElement == "create" || firstElement == "insert") {
        if(idDroit == 0 || idDroit == 2) {
            autorized = true;
        }
    } else {
        if(idDroit = 0) {
            autorized = true;
        }
    }
    if (!queryText.trimmed().isEmpty()) {
        if(autorized) {
            QSqlQueryModel *model = new QSqlQueryModel();

            QString selectedDatabasePath = dbPathMap[currentDisplayedDbName];
            if(selectedDatabasePath.isEmpty()) {
                ui->labelErrorMessage->setText("Aucune base de données sélectionnée.");
                return;
            }

            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "TempConnection");
            db.setDatabaseName(selectedDatabasePath);
            if (!db.open()) {
                ui->labelErrorMessage->setText("Erreur lors de l'ouverture de la base de données.");
                return;
            }

            model->setQuery(queryText, db);
            if (model->lastError().isValid()) {
                ui->labelErrorMessage->setText("Erreur SQL : " + model->lastError().text());
            } else {
                ui->tableViewSQL->setModel(model);
                ui->labelErrorMessage->setText("Requête exécutée avec succès.");

                // Actualiser l'affichage de la structure de la base de données
                afficherStructureBDD(selectedDatabasePath);
            }

            db.close();
            QSqlDatabase::removeDatabase("TempConnection");
        } else {
            ui->labelErrorMessage->setText("Vous ne disposez pas des droits pour effectuer des requetes de type " + firstElement);
        }

    } else {
        ui->labelErrorMessage->setText("Veuillez saisir une requête SQL.");
    }
}


void Accueil::on_listViewDB_clicked(const QModelIndex &index) {
    QString selectedDatabaseName = dbListModel->data(index, Qt::DisplayRole).toString();
    currentDisplayedDbName = selectedDatabaseName;

    QString selectedDatabasePath = dbPathMap[selectedDatabaseName];

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "ConnectionName");
    db.setDatabaseName(selectedDatabasePath);

    if (!db.open()) {
        qDebug() << "Impossible d'ouvrir la base de données :" << db.lastError().text();
        return;
    }

    QSqlQuery query("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;", db);
    QStringList tables;

    while (query.next()) {
        tables << query.value(0).toString();
    }

    afficherStructureBDD(selectedDatabasePath);

    ui->tableComboBox->clear();
    ui->tableComboBox->addItems(tables);

    db.close();
}



void Accueil::on_tableComboBox_currentIndexChanged(int index) {
    QString selectedTable = ui->tableComboBox->itemText(index);
    QSqlDatabase db = QSqlDatabase::database("ConnectionName");
    QSqlTableModel *model = new QSqlTableModel(this, db);
    model->setTable(selectedTable);
    model->select();

    ui->tableView_2->setModel(model);
    ui->tableView_2->resizeColumnsToContents();
}

void Accueil::afficherStructureBDD(const QString &nomBDD) {
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(nomBDD);

    if (!db.open()) {
        qDebug() << "Erreur lors de l'ouverture de la base de données :" << db.lastError().text();
        return;
    }

    QStandardItemModel *model = new QStandardItemModel();
    model->setHorizontalHeaderLabels({"Nom", "Type", "Schéma"});

    QSqlQuery query(db);
    query.exec("SELECT name FROM sqlite_master WHERE type='table' AND name NOT LIKE 'sqlite_%' ORDER BY name;");

    while (query.next()) {
        QString tableName = query.value(0).toString();
        QStandardItem *tableItem = new QStandardItem(tableName);
        model->appendRow(tableItem);

        QSqlQuery pragmaQuery(db);
        pragmaQuery.exec(QString("PRAGMA table_info('%1');").arg(tableName));

        QString schemaDetails = "";

        while (pragmaQuery.next()) {
            QString columnName = pragmaQuery.value(1).toString();
            QString columnType = pragmaQuery.value(2).toString();
            QString isNotNull = pragmaQuery.value(3).toInt() == 1 ? "NOT NULL" : "";
            QString defaultValue = pragmaQuery.value(4).toString();
            QString isPk = pragmaQuery.value(5).toInt() == 1 ? "PRIMARY KEY" : "";

            QString columnSchema = QString("%1 %2 %3 %4").arg(columnName, columnType, isNotNull, isPk).trimmed();
            QStandardItem *nameItem = new QStandardItem(columnName);
            QStandardItem *typeItem = new QStandardItem(columnType);
            QStandardItem *schemaItem = new QStandardItem(columnSchema);

            tableItem->appendRow({nameItem, typeItem, schemaItem});
        }

        QSqlQuery schemaQuery(db);
        schemaQuery.exec(QString("SELECT sql FROM sqlite_master WHERE type='table' AND name='%1';").arg(tableName));
        QString tableSchemaComplete = "";
        if (schemaQuery.next()) {
            tableSchemaComplete = schemaQuery.value(0).toString();
            tableSchemaComplete.replace(QRegularExpression("\\s+"), " ");
        }

        QStandardItem *schemaItem = new QStandardItem(schemaDetails);
        tableItem->appendRow(schemaItem);

        model->setItem(model->indexFromItem(tableItem).row(), 2, new QStandardItem(tableSchemaComplete));
    }

    ui->treeView->setModel(model);
    ui->treeView->expandAll();
    ui->treeView->resizeColumnToContents(0);
    ui->treeView->collapseAll();


    db.close();
}
