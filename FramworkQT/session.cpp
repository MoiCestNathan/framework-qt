#include "session.h"
#include "application.h"

/**
 * Constructeur par défaut de la classe Session.
 * Initialise une nouvelle session.
 */
Session::Session(){}

/**
 * Destructeur de la classe Session.
 * Nettoie les ressources utilisées par la session.
 */
Session::~Session(){}

/**
 * Définit l'utilisateur connecté pour la session courante.
 *
 * @param user L'utilisateur à considérer comme connecté.
 */
void Session::setConnectedUser(const Utilisateur &user)
{
    connectedUser = user;
}

/**
 * Récupère l'utilisateur actuellement connecté dans cette session.
 *
 * @return Utilisateur L'utilisateur connecté.
 */
Utilisateur Session::getConnectedUser() const
{
    return connectedUser;
}

/**
 * Réinitialise les informations de l'utilisateur connecté.
 */
void Session::clearUser()
{
    connectedUser = Utilisateur();
}
/**
 * Tente de connecter un utilisateur avec le pseudo et le mot de passe fournis.
 *
 * @param pseudo Le pseudo de l'utilisateur.
 * @param mdp Le mot de passe de l'utilisateur.
 * @return bool Retourne vrai si la connexion est réussie, faux sinon.
 */

bool Session::login(const QString& pseudo, const QString& mdp){
    Application::getApplicationinstance().updateUserList();
    QVector<Utilisateur> userList = Application::getApplicationinstance().getUserList();
    int index = -1;
    for(int i = 0; i < userList.length(); i++)
    {
        if(userList[i].getPseudo() == pseudo && userList[i].getMdp() == mdp)
        {
            index = i;
            break;
        }
    }

    if(index != -1) {
        setConnectedUser(userList[index]);
        return true;
    }
    return false;
}

/**
 * Déconnecte l'utilisateur actuellement connecté.
 *
 * @return bool Toujours vrai dans l'implémentation actuelle.
 */
bool Session::logout()
{
    connectedUser = Utilisateur();
    return 1;
}


/**
 * Met à jour l'utilisateur connecté avec les informations les plus récentes de la liste des utilisateurs.
 */
void Session::updateUser() {
    Application::getApplicationinstance().updateUserList();
    QVector<Utilisateur> userList = Application::getApplicationinstance().getUserList();
    int index = -1;
    int i = 0;
    for(Utilisateur& utilisateur : userList) {
        if(connectedUser.getId() == utilisateur.getId()) {
            index = i;
        } else {
            i++;
        }

        if(index != -1) {
            setConnectedUser(userList[index]);
        }
    }
}
